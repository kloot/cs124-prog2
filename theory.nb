(* Content-type: application/vnd.wolfram.mathematica *)

(*** Wolfram Notebook File ***)
(* http://www.wolfram.com/nb *)

(* CreatedBy='Mathematica 8.0' *)

(*CacheID: 234*)
(* Internal cache information:
NotebookFileLineBreakTest
NotebookFileLineBreakTest
NotebookDataPosition[       157,          7]
NotebookDataLength[     11861,        297]
NotebookOptionsPosition[     11359,        274]
NotebookOutlinePosition[     11735,        291]
CellTagsIndexPosition[     11692,        288]
WindowFrame->Normal*)

(* Beginning of Notebook Content *)
Notebook[{

Cell[CellGroupData[{
Cell[BoxData[{
 RowBox[{
  RowBox[{"x", "=."}], ";", 
  RowBox[{"n", "=."}], ";", 
  RowBox[{"RSolve", "[", 
   RowBox[{
    RowBox[{"{", 
     RowBox[{
      RowBox[{
       RowBox[{"T", "[", "n", "]"}], "\[Equal]", 
       RowBox[{
        RowBox[{"7", 
         RowBox[{"T", "[", 
          RowBox[{"n", "/", "2"}], "]"}]}], "+", 
        RowBox[{"18", 
         SuperscriptBox[
          RowBox[{"(", 
           RowBox[{"n", "/", "2"}], ")"}], "2"]}]}]}], ",", 
      RowBox[{
       RowBox[{"T", "[", "x", "]"}], "\[Equal]", 
       RowBox[{
        SuperscriptBox["x", "2"], 
        RowBox[{"(", 
         RowBox[{
          RowBox[{"2", "x"}], "-", "1"}], ")"}]}]}]}], "}"}], ",", 
    RowBox[{"T", "[", "n", "]"}], ",", "n"}], 
   "]"}]}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"n", "=", "128."}], ";", 
  RowBox[{"Last", "[", 
   RowBox[{"First", "[", 
    RowBox[{"First", "[", "%", "]"}], "]"}], "]"}]}]}], "Input",
 CellChangeTimes->{{3.5419884722677107`*^9, 3.5419884728886747`*^9}, {
  3.541988548057239*^9, 3.5419886236600523`*^9}}],

Cell[BoxData[
 RowBox[{"{", 
  RowBox[{"{", 
   RowBox[{
    RowBox[{"T", "[", "n", "]"}], "\[Rule]", 
    RowBox[{
     RowBox[{"-", 
      SuperscriptBox["7", 
       RowBox[{"-", 
        FractionBox[
         RowBox[{"Log", "[", "x", "]"}], 
         RowBox[{"Log", "[", "2", "]"}]]}]]}], " ", 
     RowBox[{"(", 
      RowBox[{
       RowBox[{"6", " ", 
        SuperscriptBox["7", 
         FractionBox[
          RowBox[{"Log", "[", "x", "]"}], 
          RowBox[{"Log", "[", "2", "]"}]]], " ", 
        SuperscriptBox["n", "2"]}], "-", 
       RowBox[{"5", " ", 
        SuperscriptBox["7", 
         FractionBox[
          RowBox[{"Log", "[", "n", "]"}], 
          RowBox[{"Log", "[", "2", "]"}]]], " ", 
        SuperscriptBox["x", "2"]}], "-", 
       RowBox[{"2", " ", 
        SuperscriptBox["7", 
         FractionBox[
          RowBox[{"Log", "[", "n", "]"}], 
          RowBox[{"Log", "[", "2", "]"}]]], " ", 
        SuperscriptBox["x", "3"]}]}], ")"}]}]}], "}"}], "}"}]], "Output",
 CellChangeTimes->{
  3.5419884742514753`*^9, {3.54198851211777*^9, 3.541988624185967*^9}}],

Cell[BoxData[
 RowBox[{
  RowBox[{"-", 
   SuperscriptBox["7", 
    RowBox[{"-", 
     FractionBox[
      RowBox[{"Log", "[", "x", "]"}], 
      RowBox[{"Log", "[", "2", "]"}]]}]]}], " ", 
  RowBox[{"(", 
   RowBox[{
    RowBox[{"98304.`", " ", 
     SuperscriptBox["7", 
      FractionBox[
       RowBox[{"Log", "[", "x", "]"}], 
       RowBox[{"Log", "[", "2", "]"}]]]}], "-", 
    RowBox[{"4.117714999999993`*^6", " ", 
     SuperscriptBox["x", "2"]}], "-", 
    RowBox[{"1.6470859999999972`*^6", " ", 
     SuperscriptBox["x", "3"]}]}], ")"}]}]], "Output",
 CellChangeTimes->{
  3.5419884742514753`*^9, {3.54198851211777*^9, 3.5419886241902323`*^9}}]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{
  RowBox[{"n", "=", "128"}], ";", 
  RowBox[{"Plot", "[", 
   RowBox[{
    RowBox[{
     RowBox[{"-", 
      SuperscriptBox["7", 
       RowBox[{"-", 
        RowBox[{"Ceiling", "[", 
         FractionBox[
          RowBox[{"Log", "[", "x", "]"}], 
          RowBox[{"Log", "[", "2", "]"}]], "]"}]}]]}], " ", 
     RowBox[{"(", 
      RowBox[{
       RowBox[{"6", " ", 
        SuperscriptBox["7", 
         RowBox[{"Ceiling", "[", 
          FractionBox[
           RowBox[{"Log", "[", "x", "]"}], 
           RowBox[{"Log", "[", "2", "]"}]], "]"}]], " ", 
        SuperscriptBox["n", "2"]}], "-", 
       RowBox[{"5", " ", 
        SuperscriptBox["7", 
         RowBox[{"Ceiling", "[", 
          FractionBox[
           RowBox[{"Log", "[", "n", "]"}], 
           RowBox[{"Log", "[", "2", "]"}]], "]"}]], " ", 
        SuperscriptBox["x", "2"]}], "-", 
       RowBox[{"2", " ", 
        SuperscriptBox["7", 
         RowBox[{"Ceiling", "[", 
          FractionBox[
           RowBox[{"Log", "[", "n", "]"}], 
           RowBox[{"Log", "[", "2", "]"}]], "]"}]], " ", 
        SuperscriptBox["x", "3"]}]}], ")"}]}], ",", 
    RowBox[{"{", 
     RowBox[{"x", ",", "1", ",", "32"}], "}"}]}], "]"}]}]], "Input",
 CellChangeTimes->{{3.5419886340470037`*^9, 3.541988675147428*^9}, {
  3.541988949616856*^9, 3.541988979395896*^9}}],

Cell[BoxData[
 GraphicsBox[{{}, {}, 
   {Hue[0.67, 0.6, 0.6], LineBox[CompressedData["
1:eJwV13c8ld8fAHBkRiJkj+s+99or2XI/h3KfS8OohERGUZJIUV8lo5KMjKKM
EpJIg7IyM5L6lRHKLBmlpJLI+B1/3df79brnnvN8nnM/g+J+1M6Lg42NTYed
jW3lc0fUEzOHaB5Y9/VmeX6/NfL7PTGLinjgC3Xv8+W31shmmtO1l8kLD82D
5wduWSPRr8YavKF8oDWe9FCdZY0eWMlJOSfwQfSNmIUfFtZoawE7z/3bfDAz
91Cx2swaRfm0DNm38kHKePvDgI3W6M/YruQs8dUwFNBea65gjXpG/P/pP1oN
RValZ/3mrFDGQN4LrzF+SB1puB/60AopWgSx7V3gB8tRs78DhVYo/46FgZ2w
AOi4/jEg861Qif9QjpmJADSXxdtvyLJCrRxSYeLxArDeqVTCI9YK/aHH6r/Q
XwN3paU8lQ9bIZujgbfVzwvCQO34oW41K/SuA71XvCEI3CkXqyWUrZCzoZCw
5ANBeBIdpeJGWKED7EVnuN4LwsWgYRF2WSsUmjTqOKCxFg5U7/kcLWiFCp46
CiV0rYUprW2mRT9ZiIPtZnVIohDUZO2q/F7NQjvqLV6idCFQcVc5JFzFQumR
Y918eUIQp7GwybCchfT5tKbTyoVgUdjGIPExC/msq6ZWDAnBuUqdOyfyWegN
0XdxXlMYOBU6Pf8lslAWS8L+9EthaNmn+LTpEAtN8le5WnQJg+6n/fNu3ixk
+NrVl39QGGi+a1OWvFio3e5O1I2fwhCkFlNhsZ+FuPfql1VKrgPHByEaMw4s
5Oe3U3bh4DroK3AS/bGFhUyTEsb+4xQBvpRVVCrBQkvdKmnr14rAD11py18U
FqqRabB6ICkCrNS9GU3yLGSeN1P0SVMEdvcWtp+QZiGy3Om4laMI0AcsGxbW
sdCuQYJd4r4ISP5rM7jOgfdTK5d67CAK85NuIXOfSKTtb9e21V0UZifP+9M/
kmi65GvoqK8oHA55zrF7iETHzeSGJcNFIUx1bKyqj0SnbSPuhBWKAnWftfnd
LhJdPLlt43Z2MZhtDa7+20yiWw3D2yYKxGBM5e/NPfdJlGA2c862VAyM7znd
6ikkUVg575PyGjHQPvDhkdM9Eu0r1pK71InX02cXPPNJJHUj9LvqkhhwBUr0
XskmUXKAZLyv7XpoRUfkra6R6ALF9s3U7Ho4X6rkRwkn0Yl0T849HOJwXJqy
eiCMRAfEgw1rBcSBvOvsmn6WRJvXZN1MoIjD984+NblQErHNTfrrWIvD+8NL
fPrBJDr15qJwYKY4ZC+teVPiR6IjZ2vt/myWgDWZ08sDe/HvXRIKvrddAhhH
2XOGnEnkluKW4bZHAuz7iZhPTiSyu8c23npYAixbbWS+7SGRYTeEZiXi9T/8
5dbuIhGHVt0dckgCGn1OWcduI9HVgbqF66clQVcxXsibQaL4CWFFmyhJeMxZ
tCHeDMfz934mV7wk5Abc8Xm6Ccd7NUfi0WxJ4B7/J8RvivfXN1fa/EISdniJ
U2oMSaQaV2/7bb0U8MkLaDhuIFHNpoY8xiMpGI43i9xKJ5H6S95jTVVSsO5I
ye5IGomu79lusq1ZCjQWy9AzAscvsPd/Tn1SYGAz6byBSiLNu1N/g7ilwTNp
7JSKAokyRWWsC52k4bn7f0ZWUiQ68/X4lMQqGTBz3NG1dS2JJoMryrPWyIDd
vRy9XEESOXGzRdIlZIBv0f780hoS6VNiJDZqyEAaYfD7sQCJvu++hXY4yIBj
ZuFHldUkcql/lRR1TwYubCkRRVwkMk2jG/60l4UHdcOBYgtMVPIk/1ejiyyM
crcJJP1jIvVO1eK0g7LwpsbcSRhbZq0WHZ2WBQnJ52uF5ploIdJANCFbFvRF
3/1P7C8TVfmTPzSmZcHF4vwi+s1Em8hD+T5xcjBhky4g8Y2JnnhNemxKlYOr
Mc6vCiaZSDPCT144Ww56jGbVzLDlqgOulpXKQfgv1doDX5loSfd0JE+/HLxb
H7mhZoKJquUvu+WqycNAYsnDy6NMZDZbJDHcIg85jZG9KUNMNGJVsDuiXR44
8yO/mGJfysxLpvXJQ4BpRdjIIBN1bckSOjQlDycaHhjrYetbkZFzqxXgvYjD
u75+JmpLNfovXEcB1Oz+GG/+wETzejK+yWcUQKetdKv/OyZ65SoV4xWhACpa
MWuUsG9GSxToX1SAOxlCmwe6mGhLv+h4zxUFMJHfL7QdOyFyjadsrgJUcRVJ
6nYykVLHknPeSwXg1WfFrmtnop3+w9ZlEhQonnvNRbxmov3OTpt+ylLggpH2
rU+vmMjPskNTnUoB4WCHp7exL8o0Ct/UoMDgoyVbGvazlvzuC+YU0Oph19Vs
YyI6xd/DwZcC22w9DHe3MpGuwMTOK8coYOSobSmLDbP7LV+eoEC/HNE88oKJ
nF/ZqzDOUeDM8UKFIOyEEIMp+lUKuCZfo91oYaK5t0un/tRQ4Bu/q8hCExNx
Pzvpq91IgaCNM7Kt2KL5P1wOtVLgc/vt5FRszTMfYaCTAi5+ac/1sd1Vm7ib
Jihgn6RGDW7E8Q2PTUwRUQRR/8ozgs+ZKFNXpmDjQUVwzfMYO1/HRA8O6IqK
+SqCI7ejkid2fZrVmRl/RejwOVGBsMeWg+2enFIEHpH5nsVaJtJu65o3iFOE
6dHJ7yHYdZ7x1ialitDc27rpXA0TdV7LK5WuUATFq+6xHtijrc/kF6oVwe3A
JTVL7NU6k7+qWhQhmf+onQC23SKZbtanCO6XBszTq/F9SuH4hlZRIa+v71jr
Myb60yLpoMhLhb6RoeiH2LwL2nUca6hg+0NlMhVbw901pWE9FRyMwuO8sU9o
Vm3aokqFfA5ZC/6V7zcHxZG2VLjK8tHdV8VEUvOX/yrvpsKRTIu3LGx1jRx3
PmcqJNXtK9XDtk1q12/1pEJorOsOQezrrlqD1sFUiJJpb6ivZCK1v+NaO7Ko
YC4fEWKIHTx5yjc1hwptP+t4lLCfDwncHb5LBd0I6REx7H0vtBWPl1AhwWQ2
6ncFft/Xg0XTXlChu2LbgVLsvrjVth9fU8Hiw16XPGyViPRYtU4qeL9i3LyG
XX+4lrt6AD/fAw/e09i/TXnnPv6iwp8zB5y2YIPOdT31OSp85d2Vb4gdS1MP
CFqiQnHACEMdmy5o85WHj4A09XYrUew9g9f61eUIeFJztGWsnIlyOlSkTlAJ
+Ptowrkfe7q5cneNMgF8lSP6HdiXHgz+z1aXgA937OtrsKvOKdWfIAmI1+pT
SsfmPVG+VLONgN5l6o8k7F2HrE347AkoDB5cjMH+bne05IYLAWJ/i8f/wzZm
sk+PuBPwYnZzYxD2BZMkDU1vAjyvbBv1w1YgnubVBhBQdqbq935sXwnWJ75g
AsZ+5Qw7Y5cLfJC3DyXg8Baudbux7WaWUj9fIGCmYKOpNXbWREKXZixev3hB
zxJ7sl9xXXAiAbl3nY8h7Kgmy5jVGQT0e8UXG2K3V/Q022cTYNUc9HAjtlzx
Ic6MOwQwqRO/tbEP316A0UIC/LcUhWhgl12LC9V6RMCi0XmGKjbnZYWK4KcE
uG8ztFTCtg179KeuigApgYuxBHbm8c26/PUEIHNjMUXsL97vju5sJsD+n0i/
PLa+i3dhRhs+77n3I7LYEbbz46NvCcjaZ68ug/1my2WadjcBMY6bH0thyxjL
uYf0ESAXHBgiie2t+SCzfpiAzMW8MxLYpYrmH/jHCHhzKLdeHJtDvFN81yQB
JaF6W1a8nf/AzsxpArh05LhX/EhKr/31HwIietI5Vyymymmz/I+A+1GNZisO
Nup4pcVOg/7v7iUr7iOzrd24adDDDl4r+zH2HHuRwE+DI5+UbFfOk30QmHVC
NPCe+nxy5bzcJ9c2TovRgK1sS680ts/5AXNFaRpMc4kErTxvW0pRrZ0CDV52
S1qvxEM79z+zCBoNxNzpzhTspBLrqseqNGgQ5MqlYv9pkDIe0aKBfGi4Mh3b
qWPiqageDVzOHf6sjP3sY5neFmMaXCNiP6hhR7I76ORtpoFkQHHABuwxIXrx
OxYNBnWtRfSxrRRm1Hl20ECqYdekEbYwI0nZ25EGPx5MWZpjH9/unpe6jwZ/
6qVbVu5Tj4sO8cKDBk95f4St3LfM//4nr+pHg5CGttRdK/G+nJnhFEiDn2cD
Z52wvW4ckY4JpkHU0tJlN2z1Cn7xyXAanP3Fsdd35T7PMgXvX6PBmF1Y5PmV
98sjfnkgnQbzjmtSYrHD1o/yrc2mQfcp9g/J2JZ6kVz+hTSQfeHBnYPdGVC7
oFOH401f6HqObRAef8q9iQbqJoWXX2Ffv7Lvb+JLGpgU2Z58h73/wcKvX100
GDZpHhtf+X9+M/xa+oUGu+f8tYUqVvIxj8/oFA3o9wbOSWGXCnSPrp+hQfbJ
D1w07P/Ugj6eXKaBY3x7jjE2n8/DXiNROkhemzbyxiZGVFqebaLDv5MON9qw
L5tcq042p4PyhpcSvdgziZylh5l08OQYr/mM3YQGb0na0uFkx9VLy9gHs5JP
B3nSIegkTVAP588Cp2VNjRg62O7a+vAO9rpHvjTOBDp0eL8vf4J9mu+99Idk
OjRx8HxpxN5aVsIbnUmHKzrPakewv4kd+jjykA6zVzU2Ezifa7/tTEnvocMx
l4ULBdipyuaXA/vpsLG9+18lNntYcbjVRzrEGHmmvMJu17x09O9XOlQjCaNp
7MDLDNbOZTqENBYxjHF9ecIsWOCnK0Hm5wmtDuxN1Wc9TgcqwfCVl53yuL4N
vTuoXhmsBB4Hs28bYIdP7ZiZD1WCN+G3ru/AblFQuHDqohKclUpgC1uphxG1
BSEZSnDo7an8j9jeJPvPk81K8PHCDutiXG+T28PDjksrQ3JW9pxnPc5vo1EZ
RxqUYd5SiWGC638yU2DQvkUZxPi/TDpim+YnKhi/UoZH53ieBmPH+GTd5u5W
BlrxfNYTbNXJsrtZX5SBc5NrvW4z3m/6a+lbYRUw12Tq6+J+5PM/29d6bioQ
IhbItMT9zICQ3PLiggosZ4fdL8P9k1f0geV6GzVY2BP9PLEX92fNM9v3ZKqD
XzUbLe0zzqc84k0xXRrgeuLuwfZpJtJJGf/wi64Ft97kKMwvM5HFujyBRW1t
KGtZFTKC++kW03923i+1we1tG0VEhkTX4vLHvwXoQFeFveKMGon4huUSzddu
ANULbqSKCYkanb5deli2AaI3Uu7PWZPI2cD1Hq+rLlSFvhesw/NNkuj1Qc15
XQiO2OiY6ksiMUkNj6PZG8Gf/QWawvOSkVz0/BmGHijHzfwyiSORwfmL5Sd6
9SAkxH2ZlUmis2JDzRfD9EHa75vt82IS+eSyffGRMQBBjTtZUrUkytgb4uzV
aABJpoeoHW9JFChaOaTlYQiXRkQ0XfC8SdW6Gftx0RDijI/djftJItlj/dHF
uUbwKN3rohgnC5U//pRmbW4MFQGaul6iLHRT1nyOfdIYTkUaBHHQWcj1i0XY
zUQT+Mk5Y2hiwEJ1TwWlBTVNAeJKyoyYeN4t1OG6+s4UcojaqdN7WKjyluyq
b4GbgK/ptU0Entc/zRUqqwuawawY4xMRzEK7Q5IzokrNoGGv0DP2aBbymdtM
v2/LAMZTfc68VBbK9D935OBOBmSqVOR9xW4fe1ai4MCABjEvpk4ant/f6Vsk
7WWA+g3tiCpsocfKbsEHGeBEpI20X2ehJ74C181DGeDCeWH7cjoLcQx1rOnO
Z+D5O/nZ7lssZOAgtCvhHgMO3i4i0rF9X29NZ91nQA5lPHoYu6uqUbXqMQMC
XtRt981mofy0p8xb1QzIlV5Vf+42C223v3HucCcDDH6lbi/MZaGI1u4WopsB
z2wqrk1jlyFRoYFeBgx1zw/o57EQRTs202aQATG1PQfrsH8LnK3U+8KA8cRV
Hu/usJBKZCXH1CQDHndIZkjns9C+f7Os/CkGiC5u63LDbp7w75GaYcDI4bXw
FXvBrUi+c5YBH9zTA7TvspB2z8SB2HkGeOZa3g7C9tpBv2+5iM8rINhegX29
yX1meZkBvcd+Li1j/x+I7FFA
     "]]}},
  AspectRatio->NCache[GoldenRatio^(-1), 0.6180339887498948],
  Axes->True,
  AxesOrigin->{1., 3.1*^6},
  PlotRange->{{1, 32}, {3.1094220360365524`*^6, 3.652433450223335*^6}},
  PlotRangeClipping->True,
  PlotRangePadding->{
    Scaled[0.02], 
    Scaled[0.02]}]], "Output",
 CellChangeTimes->{{3.5419886509162073`*^9, 3.541988675473152*^9}}]
}, Open  ]]
},
WindowSize->{720, 852},
WindowMargins->{{Automatic, 0}, {Automatic, 0}},
ShowSelection->True,
FrontEndVersion->"8.0 for Mac OS X x86 (32-bit, 64-bit Kernel) (February 23, \
2011)",
StyleDefinitions->"Default.nb"
]
(* End of Notebook Content *)

(* Internal cache information *)
(*CellTagsOutline
CellTagsIndex->{}
*)
(*CellTagsIndex
CellTagsIndex->{}
*)
(*NotebookFileOutline
Notebook[{
Cell[CellGroupData[{
Cell[579, 22, 1055, 33, 64, "Input"],
Cell[1637, 57, 1092, 33, 52, "Output"],
Cell[2732, 92, 654, 20, 52, "Output"]
}, Open  ]],
Cell[CellGroupData[{
Cell[3423, 117, 1350, 39, 97, "Input"],
Cell[4776, 158, 6567, 113, 233, "Output"]
}, Open  ]]
}
]
*)

(* End of internal cache information *)
