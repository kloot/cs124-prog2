\documentclass[solution, letterpaper]{prog_asst}
\usepackage{dsfont}
\usepackage{algorithmic}
\usepackage{setspace}
\usepackage{listings}
\usepackage{courier}
\lstset{
         basicstyle=\footnotesize\ttfamily,
         numberstyle=\tiny,
         numbersep=5pt,
         tabsize=2,
         extendedchars=true,
         breaklines=true,
         keywordstyle=\color{red},
            frame=b,
         stringstyle=\color{white}\ttfamily,
         showspaces=false,
         showtabs=false,
         xleftmargin=17pt,
         framexleftmargin=17pt,
         framexrightmargin=5pt,
         framexbottommargin=4pt,
         showstringspaces=false     
 }

%%%%%%%%%%%%%
% These are a few commands which I have found useful over the years..
%%%%%%%%%%%%%
\newcommand{\norm}[1]{\left|\left|#1\right|\right|}
\newcommand{\abs}[1]{\left|#1\right|}
\newcommand{\bx}{\mathbf{x}}
\newcommand{\bu}{\mathbf{u}}
\newcommand{\floor}[1]{\lfloor #1 \rfloor}
\newcommand{\ceil}[1]{\lceil#1 \rceil}
\newcommand{\bv}{\mathbf{v}}
\newcommand{\bbZ}{\mathds{Z}}
\newcommand{\bbR}{\mathds{R}}
\newcommand{\bbC}{\mathds{C}}
\newcommand{\bbN}{\mathds{N}}
\newcommand{\bbQ}{\mathds{Q}}
\newcommand{\bbzw}{\mathds{Z}[\omega]}
\newcommand{\bbzi}{\mathds{Z}[i]}
\newcommand{\nZ}[1][n]{\ensuremath{\mathds{Z}/#1\mathds{Z}}}
\newcommand{\<}{\langle}
\renewcommand{\>}{\rangle}

%% Please fill in your name and collaboration statement here.
\newcommand{\studentName}{Bannus Van der Kloot and Paul Bowden}
\newcommand{\className}{Computer Science 124}

\setstretch{1.5}

\begin{document}
\begin{singlespace}
\header{1}{Mar 1, 2012}
\end{singlespace}
\section{Code Design}
The main two design decisions made in implementing Strassen's algorithm were how to get Strassen's working with non-powers of two, and how to efficiently perform the algorithm.  In regards to performance, we decided on creating the original two matrices we are multiplying a single time, followed by utilizing those existing numbers throughout Strassen's algorithm.  We utilize 9 pointers, seven of which store the results of the 7 steps of Strassen's, while two of these are used in order to store the results of matrix subtractions and additions during Strassens -- for example, when a step requires $B − D)(G + H)$, the two store the results of $B - D$ and $G + H$, which are then run through Strassen's again.  Otherwise, we simply use a pointer to the correct location in our original matrix with the correct size -- for example, in the first step of Strassen's we do $A(F - H)$ -- $A$ is the upperleft most quadrant of the first matrix, so we pass to Strassen's the top left of our first matrix, the result of $F - H$, and the matrices passed is given half the dimensions of the current matrices.  This continues until we reach the crossover point, where regular matrix multiplication will take over and calculate the smaller matrix multiplications.  By utilizing solely the original two matrices we desire to multiply, we are being extremely space efficient.

In order to make Strassen's to work with non-powers of two, we simpy padded matrices with zeroes until they were powers of two.  Of course, this has the consequence of making matrices much larger than they otherwise might have to be.  For instannce, if our matrix dimensions are 1025, it will pad up to 2048.  Because it was overwhelmingly simple and easy to do it this way, we decided that the effect of having worst case $2n - 1$ dimensions added was not detrimental enough to improve by, for example, padding with zeroes whenever a division is made that ends up with an odd dimension.  We shortly attempted implementing this (our only real difficulty) before discarding it.

We tested for several random casees -- matrices filled with $-1$, $0$, and $1$, $0$'s and $1$s, and random numbers from $-5$ to $5$.  While we could have tested things during coding, for example checking to make sure that our matrices being multiplied together were correct directions, we did not in order to spare speed. For all tests we compared our output to  \textit{Mathematica}'s output -- in all cases, we matched their output, and likewise ran at a speed of less than 50 percent slower, which we considered a small victory.

For our matrix multiplication, we took advantage of running for loops in $i$, $k$, $j$ order in order to reduce cache misses and thus increase speeds, where $i$ and $k$ are the dimensions of the first matrix, $k$ and $j$ of the second, and $i$ and $j$ are the dimensions of the resulting matrix.  Likewise, we stored a temporary variable with values from matrices which are multiplied many times, to reduce our memory accesses (because during multiplication, one value is the same multiplied throughout the dimensions of another).

\section{Finding $n_0$}
We were tasked with finding this crossover point $n_0$ both analytically and experimentally. In our planned formulation of the problem, we would pad a matrix whose dimension was not a power of 2 with zeroes to make it a power of 2, so we took this into account when solving the recurrence. The recurrence for Strassens involves solving 7 subproblems of size $n/2$, and the work in each step involves 18 additions or subtractions of matrices of size $n/2$. We used \textit{Mathematica} to solve the recurrence:

\includegraphics[width=300px]{RSolve.png}

We adapted this to:

\[
    7^{1+\ceil{\log_2{n}}} - 6 \cdot (2^{\ceil{\log_2{n}}})^2
\]

To fit the total number of operations in our implementation. We later added a counter to our implementation that confirmed this function to be correct. The naive multiplication algorithm involves $n$ multiplications and $n-1$ additions to determine each of the $n^2$ cells, resulting in $n^2(2n-1)$ total operations. At first, we thought of finding the point at which which function is equal to the solution to Strassen's recurrence, but really we wish to determine when it makes sense for $T(n/2)$ to be $(n/2)^2(n-1)$ rather than continuing with the Strassen's recurrence. We are not solving for when Strassens alone is faster than the naive, but rather when the combination of the two is fastest. Luckily, \textit{Mathematica} can solve this recurrence generally, for a variable base case. Our recurrence will be:

\[
   T(n) = 7 T(n/2) + 18(n/2)^2
\]

With base case:

\[
   T(x) = x^2 (2x-1)
\]

Mathematica gave the following output, which we graphed:

\includegraphics[width=400px]{graph.png}


As expected, it behaves the same independent of what $n$ value we choose. Since we are using the padding technique, we are looking for the power of 2 that results in the lowest number of calculations according to this function. Therefore, the theoretical crossover point is $n_0 = 8$.

When running our code, we found an actual $n_0$ value of 64. This is likely the result of Strassen's not being able to take advantage of cache effects as well as the naive algorithm, as well as potential overhead from recursion. Therefore, a single operation in Strassen's algorithm is not equivalent to an operation of the naive algorithm. We can see this in the following output:

\lstinputlisting{operation_output}

We note that our operations counter matches our experimental analysis in that it reports minimal operations at $n_0 = 8$. However, the CPU time actually reaches a minimum at $n_0 = 64$. The operations are the same for Strassens and the naive algorithm, so we know that the culprit here must be caching in the naive algorithm making it faster or recursion overhead making Strassen's slower. We tested on both our local computers and the cloud and found that $n_0 = 64$ was appropriate on both machines. It would be interesting to try machines with varying cache sizes to see if it has an effect on $n_0$.

\end{document}
