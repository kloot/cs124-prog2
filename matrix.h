/****************************************************************************
* matrix.h
*
* Computer Science 124
* Programming Assignment 2
*
* Declares a matrix's functionality.
***************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef int mat_type;

typedef struct matrix
{
    mat_type **mat;
    int row;
    int col;
    int n; // always power of 2
    int display; // actual size of matrix
} matrix;

int upper_power_of_2(int n);

matrix init_matrix(int n);
void zero_matrix(matrix a);
void free_matrix(matrix a);
matrix upper_left(matrix a);
matrix upper_right(matrix a);
matrix lower_left(matrix a);
matrix lower_right(matrix a);

void add(matrix a, matrix b, matrix c);
void subtract(matrix a, matrix b, matrix c);
void multiply(matrix a, matrix b, matrix c);
void print(matrix a);
void print_diag(matrix a);

void file_to_matrices(FILE *matrix_file, matrix a, matrix b);