/****************************************************************************
* matrix.c
*
* Computer Science 124
* Programming Assignment 2
*
* Implements a matrix's functionality.
***************************************************************************/

#include "matrix.h"

extern int operations;

int upper_power_of_2(int n)
{
    n--;
    n |= n >> 1;
    n |= n >> 2;
    n |= n >> 4;
    n |= n >> 8;
    n |= n >> 16;
    n++;
    return n;
}

matrix init_matrix(int n)
{
    matrix a;
    a.row = 0;
    a.col = 0;
    a.n = upper_power_of_2(n);
    a.mat = calloc(a.n, sizeof(int*));
    for (int i = 0; i < a.n; i++)
        a.mat[i] = calloc(a.n, sizeof(int));
    a.display = n;
    return a;
}

void zero_matrix(matrix a)
{
    for (int i = 0; i < a.display; i++)
        for (int j = 0; j < a.display; j++)
            a.mat[i][j] = 0;
    return;
}

void free_matrix(matrix a)
{
    for (int i = 0; i < a.n; i++)
        free(a.mat[i]);
    free (a.mat);
}

matrix upper_left(matrix a)
{
    matrix b = {
        a.mat,
        a.row,
        a.col,
        a.n / 2,
        a.n / 2
    };
    return b;
}

matrix upper_right(matrix a)
{
    matrix b = {
        a.mat,
        a.row,
        a.col + a.n / 2,
        a.n / 2,
        a.n / 2
    };
    return b;
}

matrix lower_left(matrix a)
{
    matrix b = {
        a.mat,
        a.row + a.n / 2,
        a.col,
        a.n / 2,
        a.n / 2
    };
    return b;
}

matrix lower_right(matrix a)
{
    matrix b = {
        a.mat,
        a.row + a.n / 2,
        a.col + a.n / 2,
        a.n / 2,
        a.n / 2
    };
    return b;
}


void add(matrix a, matrix b, matrix c)
{
    for (int i = 0; i < a.n; i++)
        for (int j = 0; j < a.n; j++) //,operations++)
            c.mat[i+c.row][j+c.col] = a.mat[i+a.row][j+a.col] +
                                      b.mat[i+b.row][j+b.col];
}


void subtract(matrix a, matrix b,matrix c)
{
    for (int i = 0; i < a.n; i++)
        for (int j = 0; j < a.n; j++) //, operations++)
            c.mat[i+c.row][j+c.col] = a.mat[i+a.row][j+a.col] -
                                      b.mat[i+b.row][j+b.col];
}

// i k j order to reduce cache misses
void multiply(matrix a, matrix b, matrix c)
{
    for (int i = 0; i < a.n; i++)
        for (int k = 0; k < a.n; k++) //, operations--)
            for (int j = 0, tmp = a.mat[i+a.row][k+a.col]; j < a.n; j++) //, operations += 2)
                c.mat[i+c.row][j+c.col] += tmp * b.mat[k+b.row][j+b.col];
}

void print(matrix a)
{
    int n = a.display;
    for (int i = 0; i < n; i++)
    {
        printf("| ");
        for (int j = 0; j < n; j++)
            printf("%d\t", a.mat[i+a.row][j+a.col]);
        printf("|\n");
    }
    printf("\n");
}

void print_diag(matrix a)
{
    for (int i = 0; i < a.display; i++)
        printf("%d\n", a.mat[i][i]);
}

void file_to_matrices(FILE *matrix_file, matrix a, matrix b)
{
    for(int i = 0; i < a.display; i++)
        for (int j = 0 ; j < a.display; j++)
            fscanf(matrix_file,"%d",&a.mat[i][j]);

    for(int i = 0; i < a.display; i++)
        for (int j = 0 ; j < a.display; j++)
            fscanf(matrix_file,"%d",&b.mat[i][j]);
}