#
# Makefile
#
# Computer Science 50
# Problem Set 6
#


# compiler to use
CC = gcc

# flags to pass compiler
CFLAGS = -std=c99 -Wall -Werror -Wformat=0  -O3 -funroll-loops
# -O3 -funroll-loops

# name for executable
EXE = strassen

# space-separated list of header files
HDRS = matrix.h strassen.h

# space-separated list of libraries, if any,
# each of which should be prefixed with -l
LIBS = -l m

# space-separated list of source files
SRCS = matrix.c strassen.c main.c

# automatically generated list of object files
OBJS = $(SRCS:.c=.o)


# default target
$(EXE): $(OBJS) $(HDRS)
	$(CC) $(CFLAGS) -o $@ $(OBJS) $(LIBS)

# dependencies 
$(OBJS): $(HDRS) Makefile

# housekeeping
clean:
	rm -f core $(EXE) *.o
