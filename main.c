/***************************************************************************
* main.c
*
* Computer Science 124
* Programming Assignment 2
*
* Runs Strassen's algorithm
*
* Usage: strassen debug dimension inputfile
*
* Debug: 0 = diagonal results only
* 1 = print both input matrices and output matrix
* 2 = determine optimal n0 value
* other = print only time
* note: must uncomment operations in matrix.c if you want to see #s
***************************************************************************/

#include <stdio.h>
#include <time.h>

#include "strassen.h"

// size of matrix to recurse down to in strassens
int n0 = 64;
int operations = 0;

int main(int argc, char const *argv[])
{
    // error checking
    if (argc != 4)
    {
        printf("Usage: strassen debug dimension inputfile\n");
        return -1;
    }
    int debug = atoi(argv[1]);
    int n = atoi(argv[2]);
    FILE *matrix_file = fopen(argv[3], "r");
    
    if (matrix_file == NULL)
    {
        printf("Invalid file name\n");
        return -1;
    }

    // initialize 2d matrices
    matrix a = init_matrix(n);
    matrix b = init_matrix(n);
    matrix c = init_matrix(n);

    // load a and b with data from input file
    file_to_matrices(matrix_file, a, b);

    // timing
    clock_t t0, t1;

    // multiply a and b, storing result in c
    t0 = clock();
    strassen(a, b, c);
    t1 = clock();

    float cpu_time = (float) (t1 - t0) / CLOCKS_PER_SEC;

    // only print diagonal values if debug is 0
    if (debug == 0)
        print_diag(c);
    else
    {
        // print both input matrices and output matrix
        if (debug == 1)
        {
            print(a);
            print(b);
            print(c);
        }
        printf("CPU time: %f\n", cpu_time);
        printf("n0: %d\n", n0);

        // determine optimal n0
        if (debug == 2)
        {
            float prev_time = 9999;
            while (prev_time > cpu_time)
            {
                prev_time = cpu_time;
                n0 *= 2;

                t0 = clock();
                strassen(a, b, c);
                t1 = clock();

                cpu_time = (float) (t1 - t0) / CLOCKS_PER_SEC;
            }
            printf("Optimal CPU time: %f\n", prev_time);
            printf("Optimal n0: %d\n", n0 / 2);
        }
        printf("operations: %d\n", operations);
    }

    // free all memory
    free_matrix(a);
    free_matrix(b);
    free_matrix(c);
    fclose(matrix_file);

    return 0;
}
