/****************************************************************************
* strassen.h
*
* Computer Science 124
* Programming Assignment 2
*
* Declares Strassen's algorithm functionality.
***************************************************************************/

#include "matrix.h"

void strassen(matrix a, matrix b, matrix c);