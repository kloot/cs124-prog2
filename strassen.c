/****************************************************************************
* strassen.h
*
* Computer Science 124
* Programming Assignment 2
*
* Implements Strassen's algorithm functionality.
***************************************************************************/

#include "strassen.h"

extern int n0;

void strassen(matrix a, matrix b, matrix c)
{
    if(a.n <= n0)
    {
        multiply(a, b, c);
    }
    else
    {
        matrix p[9];

        // p[0] and p[8] are for temp storage
        for (int i = 0; i < 9; i++)
            p[i] = init_matrix(a.n / 2);

        // | A B | | E F |
        // | C D | | G H |

        // p[1] = A(F - H)
        subtract(upper_right(b), lower_right(b), p[0]);
        strassen(upper_left(a), p[0], p[1]);
        zero_matrix(p[0]);

        // p[2] = (A + B)H
        add(upper_left(a), upper_right(a), p[0]);
        strassen(p[0], lower_right(b), p[2]);
        zero_matrix(p[0]);

        // p[3] = (C + D)E
        add(lower_left(a), lower_right(a), p[0]);
        strassen(p[0], upper_left(b), p[3]);
        zero_matrix(p[0]);

        // p[4] = D(G − E)
        subtract(lower_left(b), upper_left(b), p[0]);
        strassen(lower_right(a), p[0], p[4]);
        zero_matrix(p[0]);

        // p[5] = (A + D)(E + H)
        add(upper_left(a), lower_right(a), p[0]);
        add(upper_left(b), lower_right(b), p[8]);
        strassen(p[0], p[8], p[5]);
        zero_matrix(p[0]);
        zero_matrix(p[8]);

        // p[6] = (B − D)(G + H)
        subtract(upper_right(a), lower_right(a), p[0]);
        add(lower_left(b), lower_right(b), p[8]);
        strassen(p[0], p[8], p[6]);
        zero_matrix(p[0]);
        zero_matrix(p[8]);

        // p[7] = (A − C)(E + F)
        subtract(upper_left(a), lower_left(a), p[0]);
        add(upper_left(b), upper_right(b), p[8]);
        strassen(p[0], p[8], p[7]);
        zero_matrix(p[0]);
        zero_matrix(p[8]);

        // upper left
        add(p[4], p[5], upper_left(c));
        subtract(upper_left(c), p[2], upper_left(c));
        add(upper_left(c), p[6], upper_left(c));

        // upper right
        add(p[1], p[2], upper_right(c));

        // lower left
        add(p[3], p[4], lower_left(c));

        // lower right
        add(p[5], p[1], lower_right(c));
        subtract(lower_right(c), p[3], lower_right(c));
        subtract(lower_right(c), p[7], lower_right(c));

        for (int i = 0; i < 9; i++)
            free_matrix(p[i]);

        return;
    }
}