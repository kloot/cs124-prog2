import random

d = 512

f = open('input.txt', 'w')
f2 = open('mathematica.txt', 'w')
write = f.write
write2 = f2.write

write2("{")
for i in xrange(d):
    randoms = [str(random.randrange(-5,6)) for j in xrange(d)]
    write("\n".join(randoms))
    write('\n')
    write2("{")
    write2(",".join(randoms))
    write2("},")
write2("}")

print "done"
write2(".")

write2("{")
for i in xrange(d):
    randoms = [str(random.randrange(-5,6)) for j in xrange(d)]
    write("\n".join(randoms))
    write('\n')
    write2("{")
    write2(",".join(randoms))
    write2("},")
write2("}")

print "done"